from random import randint

import png
import math
from tqdm import tqdm as bar

# Given 2D array of seed positions

# Need to calculate distance from each point in plane array
# to closest seed using Euclidean distance

# Value of 2D array at location should be index value for
# closest possible seed in array of seeds

def euclid_distance(point1, point2):
    length = point2[0] - point1[0]
    height = point2[1] - point1[1]
    
    return math.sqrt(length*length + height*height)


def manhattan_distance(point1, point2):
    return abs(point1[0] - point2[0]) + abs(point1[1] - point2[1])

class Seed:

    x = 0
    y = 0
    neighbours = []

    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.neighbours = [[self.x-1, self.y+1], [self.x, self.y+1], [self.x+1, self.y+1], [self.x-1, self.y], [self.x+1, self.y], [self.x-1, self.y-1], [self.x, self.y-1], [self.x+1,  self.y-1]]



    def reset(self):
        self.neighbours = [[self.x-1, self.y+1], [self.x, self.y+1], [self.x+1, self.y+1], [self.x-1, self.y], [self.x+1, self.y], [self.x-1, self.y-1], [self.x, self.y-1], [self.x+1,  self.y-1]]



class Voronoi:

    width = 0
    height = 0
    plane = []
    seeds = []


    def __init__(self, width, height, seeds=[]):
        
        self.width = width
        self.height = height
        self.seeds = seeds

        for i in range(self.height):
            self.plane.append([])
            for j in range(self.width):
                self.plane[i].append(".")

    def partition(self):

        for i in range(self.height):
            
            for j in range(self.width):

                closest_seed = 0
                seed_distance = 100000000000

                for x in range(len(self.seeds)):

                    seed = self.seeds[x]
                    self.plane[seed.y][seed.x] = "X"


                    distance = manhattan_distance([seed.x, seed.y], [j, i])

                    if distance < seed_distance:
                        closest_seed = x
                        seed_distance = distance

                self.plane[i][j] = closest_seed

    def adv_partition(self):

        for i in range(len(self.seeds)):
    
            seed = self.seeds[i]
            self.plane[seed.y][seed.x] = "X"

        new_found = 1

        while new_found > 0:

            new_found = 0

            for i in range(len(self.seeds)):

                seed = self.seeds[i]

                for y in range(len(seed.neighbours)):

                    n = seed.neighbours[y]

                    if(n[0] >= 0 and n[0] < self.width and n[1] >= 0 and n[1] < self.height):

                        if(self.plane[n[1]][n[0]] == "."):
                            # print(n[1], n[0])
                            self.plane[n[1]][n[0]] = i
                            # print("Tile taken by seed %d" % (i))
                            new_found += 1

                        new_neighbours = [[n[0]-1, n[1]+1], [n[0], n[1]+1], [n[0]+1, n[1]+1], [n[0]-1, n[1]], [n[0]+1, n[1]], [n[0]-1, n[1]-1], [n[0], n[1]-1], [n[0]+1,  n[1]-1]]


                        for pos in new_neighbours:

                            try:
                                if(pos[0] >= 0 and pos[0] < self.width and pos[1] >= 0 and pos[1] < self.height):

                                    if(self.plane[pos[1]][pos[0]] == "." and pos not in seed.neighbours):
                                        seed.neighbours.append(pos)
                            except:
                                pass



    def add_seed(self, seed):

        self.seeds.append(seed)

    def clean(self):
        for i in range(self.height):

            for j in range(self.width):

                self.plane[i][j] = "."

        for seed in self.seeds:

            seed.reset()

    def show(self):
        print("-------------------------------------")
        for row in self.plane:
            print(row)

    def generate_graphic(self, name):
        arr = []

        for i in range(self.height):
            arr.append([])
            for j in range(self.width):
                if(self.plane[i][j] == "X"):
                    arr[i].append(255)
                     
                else:
                    arr[i].append(self.plane[i][j] * 10)

        png.from_array(arr, 'L').save(name)

length = 1000
width = 1000

v = Voronoi(length, width)

for i in range(20):
    v.add_seed(Seed(randint(0, width-1), randint(0, width-1)))

v.partition()
v.generate_graphic('test.png')s